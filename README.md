# SQL Engineer Assignments

For the following assignments, create a private repo and structure your answers in Markdown, TXT and/or SQL files as appropriate. When completed, grant access to following accounts:

## For GitHub
GitHub User: array-com

GitHub Email: github@array.com

## For GitLab
GitLab User: array.com

GitLab Email: gitlab@array.com

Please note somewhere in your work how many hours you spent on the exercise.

## Assignment 1
Imagine for a moment you're helping to design the initial schema for our white label portal product that we offer to clients.

Customers log into the portal, branded to a particular client and are able to order credit reports. A total of 6 products are offered: Partial and Full reports for each of the three credit bureaus (Equifax, Experian, and TransUnion). We then bill our clients based on the number of each report their customers order.

Describe the data schema for these entities. Include entity names, properties, data types, relationships and any relevant indexes, keys and constraints.

## Assignment 2
Not long after the initial launch, the CTO comes to you and asks to build a report showing the number of 3B TransUnion reports ordered per month, for each client.

Using the schema you designed above, what would be the query you'd use to retrieve this information?

## Assignment 3
One of our clients provides a data dump of customers they'd like us to add to our system. The format is a .csv file with 1,345 lines. 

Describe your approach in validating, formatting and importing the data into your data storage. Detail the tools and SQL statements you'd execute.

## Assignment 4
You've been asked to demonstrate to a junior engineer the use of CTEs. Write a query that would identify clients that have done over 100 reports in the last 30 days. The query should return the client's name and the number of reports within the last 30 days.

## Assignment 5
You discover the SQL database performance is being impacted by many concurrent long running queries.

Describe your approach in how you'd diagnose, test and resolve the issue. Detail the tools and SQL statements you'd execute.
